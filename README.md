# TODO ESTUCHE ERP. 

Sistema de ERP para la empresa TODO ESTUCHE. 

### Pre-requisitos 📋

_Antes de comenzar, necesitas los siguiente requerimientos minimos instalados en tu equipo para poder correr el proyecto_

```
MYSQL 5.7
Composer
```

### Instalación 🔧

_Clona el repositorio y ejecuta los siguientes comandos en el interior de la carpeta del proyecto._

```
Composer install
cp .env.example .env 
php artisan key:generate 
php artisan migrate --seed 
npm install  
php artisan serve
```

><b>NOTA</b>: Recuerda haber colocado las credenciales de tu BD en el .env antes de correr las migraciones.  

_Y en caso de que ya tengas las migraciones ejecutadas y solo quieres incluir las nuevas_

```
php artisan migrate:refresh --seed
```

## Modulos Disponibles ⚙
* User
* People
* Lead
* Contact
* Company
* Product
* Quotes
* Setting

_Pendiente anexar documentacion_

## Despliegue 📦

_En construccion_

## Licencia 📄

_Pendiente especificar Licencia_
