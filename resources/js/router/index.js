import Vue from 'vue'
import VueRouter from 'vue-router'

import HomePanel from '../components/dashboardViews/HomePanel.vue'
import UsersCreater from '../components/dashboardViews/UsersCreater.vue'
import TestPanel from '../components/dashboardViews/TestPanel.vue'
import ClientPanel from '../components/dashboardViews/ClientPanel.vue'


Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: HomePanel
        },
        {
            path: '/UsersCreater',
            name: 'UsersCreater',
            component: UsersCreater
        },
        {
            path: '/TestPanel',
            name: 'TestPanel',
            component: TestPanel
        },
        {
            path: '/ClientPanel',
            name: 'ClientPanel',
            component: ClientPanel
        },
    ]
})

// router.beforeEach((to, from, next) => {}

export default router