import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		setting:{}
    },
    getters:{
        getUserLevels(state){
            return state.setting.levels
        }
    },
	mutations: {
		SET_SETTING(state, res){
			state.setting = res.data
		}
	},
	actions:{
		fetchSetting(context){
			axios({
				method:'get',
				url: '/setting/',
			}).then(res => {
				context.commit('SET_SETTING', res)
			})
		}
	}
})

export default store