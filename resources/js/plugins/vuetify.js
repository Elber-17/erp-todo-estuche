import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
// #34C3F2#34C3F2
Vue.use(Vuetify)

const vuetify = new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#d8ae4f',
                secondary: '#507AD9',
                acent:'#F2D468',
                base1: '#E8DCC5',
                base: '#FFF9E5',
                base2: '#FFEED9',
                secondaryBase: '#F0E2AF',
                dark:'#25292c',
                darkSecondary: '#736531'
            },
             icons: {
                 iconfont: 'mdi', // default - only for display purposes
             },
        },
    },
})

export default vuetify