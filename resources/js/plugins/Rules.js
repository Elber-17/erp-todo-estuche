let Rules = {
    required: value => !!value || 'Requerido.',
    email: value => {
            const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            return pattern.test(value) || 'Email invalido.'
        },
    allowSpaces: v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed'
}

export default Rules