@extends('layouts.basic-template')

@section('title', 'Ingreso')

@section('content')
    {{--
        Esto es un comentario, siempre que llames a un componente de vue debes llamarlo
        dentro de una etiqueta div y que el atributo id sea 'vue-app'
    --}}

    @php
        $alertType = 'none';
        $alertMsg = 'none';
    @endphp

    @if ($alert = Session::get('alert'))
        @php
            $alertType = $alert['type'];
            $alertMsg = $alert['msg'];
        @endphp
    @endif
    

    <div id="vue-app">
        <auth alert-type="{{$alertType}}" alert-msg="{{$alertMsg}}">
            @csrf
        </auth>
    </div>
@endsection
