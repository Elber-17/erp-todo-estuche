<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\AuthController@get')->name('login');
Route::get('/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('/login', 'Auth\AuthController@post');
Route::delete('/logout', 'Auth\AuthController@logout')->middleware('auth')->name('logout');
Route::get('/dashboard', 'Auth\AuthController@getDashboard')->middleware('auth')->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::resource('users', 'User\UserController')->except(['edit', 'create']);
    Route::resource('leads', 'Lead\LeadController')->except(['edit', 'create']);
    Route::resource('contacts', 'Contact\ContactController')->except(['edit', 'create']);
    Route::post('convertLead/{id}', 'Contact\ContactController@transformContact');
    Route::resource('companies', 'Company\CompanyController')->except(['edit', 'create']);
    Route::resource('products', 'Product\ProductController')->except(['edit', 'create']);
    Route::resource('quotes', 'Quote\QuoteController')->except(['create', 'edit']);
    Route::resource('setting', 'Setting\SettingController')->except(['create', 'edit', 'destroy', 'store']);
});
