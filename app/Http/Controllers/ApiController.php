<?php

namespace App\Http\Controllers;

use App\Entities\UserLevel;
use App\Entities\UserStatus;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function GetListAll()
    {
        return response()->json(['levels' => UserLevel::all(),
                'status' => UserStatus::all()]);
    }
}
