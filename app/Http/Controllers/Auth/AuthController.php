<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth as Authentication;


class AuthController extends Controller {

    public function get(){
        return redirect()->route('login');
    }

    public function getDashboard()
    {
        return view('dashboard.dashboard');
    }

    public function getLogin(Request $request)
    {
        if (Authentication::check()) {
            return redirect()->route('dashboard');
        }

        $urlArray = explode('/',redirect()->intended()->getTargetUrl());
        $url = end($urlArray);

        if($url == 'dashboard'){
            $request->session()->flash('alert', ['type' => 'info', 'msg' => 'Debes iniciar Session primero']);

            return view('auth.auth');
        }

        return view('auth.auth');
    }

    public function post(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Authentication::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }


        return redirect()->route('login')->with('alert',['type' => 'error', 'msg' => 'Sus credenciales son invalidas']);
    }

    public function logout()
    {
        Authentication::logout();

        return redirect()->route('login')->with('alert',['type' => 'success', 'msg' => 'Ha finalizado sesion con exito']);
    }


}
