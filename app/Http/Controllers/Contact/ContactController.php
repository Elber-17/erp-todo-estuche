<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\IndexRequest;
use App\Http\Requests\Contact\StoreRequest;
use App\Http\Requests\Contact\UpdateRequest;
use App\Repositories\Contact\ContactRepository;

class ContactController extends Controller
{
    protected $contactRepository;

    /**
     * Contact Constructor.
     *
     * @param ContactRepository $contactRepository
     */

    public function __construct(ContactRepository $contactRepository)
    {
      $this->contactRepository = $contactRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->contactRepository->getAll($request->all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->contactRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->contactRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->contactRepository->UpdateById($id, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->contactRepository->destroy($id))->setStatusCode(204);
    }

    /**
     * Transforms a lead into a contact.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transformContact($id)
    {
        return response()->json($this->contactRepository->transformContact($id))->setStatusCode(200);
    }
}
