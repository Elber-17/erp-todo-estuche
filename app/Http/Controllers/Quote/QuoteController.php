<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\Controller;
use App\Http\Requests\Quote\IndexRequest;
use App\Http\Requests\Quote\StoreRequest;
use App\Http\Requests\Quote\UpdateRequest;
use App\Repositories\Quote\QuoteRepository;

class QuoteController extends Controller
{
    protected $quoteRepository;

    /**
     * Quote Constructor.
     *
     * @param QuoteRepository $quoteRepository
     */

    public function __construct(QuoteRepository $quoteRepository)
    {
      $this->quoteRepository = $quoteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        return response()->json($this->quoteRepository->getAll($request->all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return response()->json($this->quoteRepository->create($request->all()))->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->quoteRepository->getById($id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->quoteRepository->UpdateById($id, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->quoteRepository->destroy($id))->setStatusCode(204);
    }
}
