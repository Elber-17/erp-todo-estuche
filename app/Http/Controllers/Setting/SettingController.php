<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\UpdateRequest;
use App\Repositories\Setting\SettingRepository;

class SettingController extends Controller
{
    protected $settingRepository;

    /**
     * Setting Constructor.
     *
     * @param SettingRepository $settingRepository
     */

    public function __construct(SettingRepository $settingRepository)
    {
      $this->settingRepository = $settingRepository;
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->settingRepository->getModules());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->settingRepository->getById($id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json($this->settingRepository->UpdateById($id, $request->all()));
    }

}
