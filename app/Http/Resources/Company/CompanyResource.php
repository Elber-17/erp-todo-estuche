<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'industry'         => $this->industry,
            'rif'              => $this->rif,
            'website'          => $this->website,
            'phone'            => $this->phone,
            'shipping_address' => $this->shipping_address,
            'created_at'  => $this->created_at->format('Y-m-d H:s:m'),
            'updated_at'  => $this->updated_at->format('Y-m-d H:s:m'),
        ];
    }
}
