<?php

namespace App\Http\Resources\Company;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Company\CompanyResource;

class CompanyCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => CompanyResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
