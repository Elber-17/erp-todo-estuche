<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'code'             => $this->cod,
            'name'             => $this->name,
            'description'      => $this->description,
            'cost_hours_man'   => $this->cost_hours_man,
            'speed_production' => $this->speed_production,
            'elements_product' => $this->elements_product,
            'unit_price'       => $this->unit_price,
            'cant_per_boxes'   => $this->cant_per_boxes,
            'dimensions'       => $this->dimensions,
            'cover_picture'    => $this->cover_picture,
            'perforations'     => $this->perforations,
            'type_pack'        => $this->type_pack,
            'status'           => $this->status,
            'raw_materials'    => $this->materials,
            'warehouse_id'     => $this->warehouse,
            'created_at'  => $this->created_at->format('Y-m-d H:s:m'),
            'updated_at'  => $this->updated_at->format('Y-m-d H:s:m'),
        ];
    }
}
