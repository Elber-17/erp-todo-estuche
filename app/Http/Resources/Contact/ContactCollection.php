<?php

namespace App\Http\Resources\Contact;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Contact\ContactResource;

class ContactCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ContactResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
