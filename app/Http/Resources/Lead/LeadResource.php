<?php

namespace App\Http\Resources\Lead;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LeadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'source'      => $this->source,
            'description' => $this->description,
            'person'      => $this->people,
            'company'     => $this->checkCompany($this->company),
            'user'        => [
                'id'       => $this->user_id,
                'name'     => $this->user->people->fullname,
                'username' => $this->user->username],
            'created_at'  => $this->created_at->format('Y-m-d H:s:m'),
            'updated_at'  => $this->updated_at->format('Y-m-d H:s:m'),
        ];
    }

    public function checkCompany($company) {
        if(!empty($company))
        {
            return [
              'id'   => $this->company_id,
              'name' => $this->company->name
            ];
        }else {
            return null;
        }
    }
}


