<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'username'    => $this->username,
            'is_active'   => $this->is_active,
            'person'      => $this->people,
            'created_at'  => $this->created_at->format('Y-m-d H:s:m'),
            'updated_at'  => $this->updated_at->format('Y-m-d H:s:m')
        ];
    }
}

