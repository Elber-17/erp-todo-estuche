<?php

namespace App\Http\Resources\Quote;

use App\Http\Resources\ApiCollection;
use App\Http\Resources\Quote\QuoteResource;

class QuoteCollection extends ApiCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => QuoteResource::collection($this->collection),
            'meta'  => $this->dataMeta($request),
            'links' => $this->dataLinks($request)
        ];
    }
}
