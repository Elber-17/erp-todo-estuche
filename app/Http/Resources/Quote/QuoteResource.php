<?php

namespace App\Http\Resources\Quote;

use Illuminate\Http\Resources\Json\JsonResource;

class QuoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'cod'              => $this->cod,
            'valid_until'      => $this->valid_until,
            'date_delivery'    => $this->form_pay,
            'status'           => $this->status,
            'delivery_design'  => $this->delivery_design,
            'design_our'       => $this->design_our,
            'details'          => $this->details,
            'terms_services'   => $this->terms_services,
            'product_id'       => $this->product_id,
            'client'   => [
                'name'       => $this->contactPerson->fullname,
                'company'    => $this->contactCompany->basic,
            ],
            'created_at'  => $this->created_at->format('Y-m-d H:s:m'),
            'updated_at'  => $this->updated_at->format('Y-m-d H:s:m')
        ];
    }
}

