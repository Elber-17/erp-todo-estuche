<?php

namespace App\Http\Requests\Lead;

use App\Entities\Lead;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lead_id = $this->route()->parameters('leads');
        $lead = $this->findRecordInRequest(Lead::class, $lead_id['lead']);

        return [
            'first_name'        => 'sometimes',
            'last_name'         => 'sometimes',
            'dni'               => 'sometimes|unique:people,dni,'.$lead->people->id,
            'email'             => 'sometimes|unique:people,email,'.$lead->people->id,
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'source'            => 'sometimes',
            'description'       => 'sometimes',
            'person_id'         => 'required_without:dni|unique:contacts,person_id,'.$lead_id['lead'],
            'user_id'           => 'sometimes|exists:users,id',
            'company_id'        => 'sometimes|exists:companies,id'
        ];
    }
}

