<?php

namespace App\Http\Requests\Product;

use App\Entities\Product;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product_id = $this->route()->parameters('products');
        $product = $this->findRecordInRequest(Product::class, $product_id['product']);

        return [
            'cod'                       => 'required|unique:products,cod,'.$product->id,
            'name'                      => 'sometimes',
            'description'               => 'sometimes',
            'cost_hours_man'            => 'sometimes',
                'cost_hours_man.print'      => 'required_with:cost_hours_man',
                'cost_hours_man.troquel'    => 'required_with:cost_hours_man',
                'cost_hours_man.gummed'     => 'required_with:cost_hours_man',
                'cost_hours_man.operative'  => 'required_with:cost_hours_man',
            'speed_production'          => 'sometimes',
                'speed_production.print'    => 'required_with:speed_production',
                'speed_production.troquel'  => 'required_with:speed_production',
                'speed_production.gummed'   => 'required_with:speed_production',
            'elements_product'          => 'sometimes',
                'elements_product.relieve'          => 'sometimes',
                'elements_product.troquel'          => 'sometimes',
                'elements_product.caliber'          => 'required_with:elements_product',
                'elements_product.type_gummed'      => 'required_with:elements_product',
                'elements_product.sheet_fit'        => 'sometimes',
                'elements_product.unit_measurement' => 'required_with:elements_product',
                'elements_product.varnished'        => 'sometimes',
                'elements_product.colors'           => 'sometimes',
                'elements_product.others'           => 'sometimes',
            'unit_price'                => 'sometimes|numeric',
            'cant_per_boxes'            => 'sometimes',
            'dimensions'                => 'sometimes',
                'dimensions.form'       => 'required_with:dimensions',
                'dimensions.width'      => 'required_with:dimensions',
                'dimensions.height'     => 'required_with:dimensions',
                'dimensions.length'     => 'required_with:dimensions',
            'cover_picture'             => 'sometimes',
            'type_pack'                 => 'sometimes',
            'status'                    => 'required',
            'raw_materials'             => 'sometimes|array|exists:raw_materials,id',
            'warehouse_id'              => 'sometimes|exists:warehouses,id'
        ];
    }
}
