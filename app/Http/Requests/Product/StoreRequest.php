<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod'                       => 'required|unique:products,cod',
            'name'                      => 'required',
            'description'               => 'sometimes',
            'cost_hours_man'            => 'required',
                'cost_hours_man.print'      => 'required_with:cost_hours_man',
                'cost_hours_man.troquel'    => 'required_with:cost_hours_man',
                'cost_hours_man.gummed'     => 'required_with:cost_hours_man',
                'cost_hours_man.operative'  => 'required_with:cost_hours_man',
            'speed_production'          => 'required',
                'speed_production.print'    => 'required_with:speed_production',
                'speed_production.troquel'  => 'required_with:speed_production',
                'speed_production.gummed'   => 'required_with:speed_production',
            'elements_product'          => 'required',
                'elements_product.relieve'          => 'sometimes',
                'elements_product.troquel'          => 'sometimes',
                'elements_product.caliber'          => 'required_with:elements_product',
                'elements_product.type_gummed'      => 'required_with:elements_product',
                'elements_product.sheet_fit'        => 'sometimes',
                'elements_product.unit_measurement' => 'required_with:elements_product',
                'elements_product.varnished'        => 'sometimes',
                'elements_product.colors'           => 'sometimes',
                'elements_product.others'           => 'sometimes',
            'unit_price'                => 'required|numeric',
            'cant_per_boxes'            => 'required',
            'dimensions'                => 'required',
                'dimensions.form'       => 'required_with:dimensions',
                'dimensions.width'      => 'required_with:dimensions',
                'dimensions.height'     => 'required_with:dimensions',
                'dimensions.length'     => 'required_with:dimensions',
            'cover_picture'             => 'sometimes',
            'type_pack'                 => 'required',
            'status'                    => 'required',
            'raw_materials'             => 'required|array|exists:raw_materials,id',
            'warehouse_id'              => 'required|exists:warehouses,id'
        ];
    }
}
