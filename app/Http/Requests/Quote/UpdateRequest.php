<?php

namespace App\Http\Requests\Quote;

use App\Entities\Quote;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $quote_id = $this->route()->parameters('quotes');
        $quote = $this->findRecordInRequest(Quote::class, $quote_id['quote']);

        return [
            'cod'                    => 'required|unique:quotes,cod,'.$quote->id,
            'valid_until'            => 'required|date_format:d-m-Y',
            'date_delivery'          => 'required|date_format:d-m-Y',
            'form_pay'               => 'sometimes',
            'status'                 => 'required',
            'details'                => 'required|array',
            'details.subtotal'       => 'required_with:details',
            'details.IVA'            => 'required_with:details',
            'details.others'         => 'sometimes',
            'details.total'          => 'required_with:details',
            'delivery_design'        => 'sometimes|boolean',
            'design_our'             => 'sometimes|boolean',
            'product_id'             => 'required|exists:products,id',
            'contact_id'             => 'required|exists:contacts,id',
            'terms_services'         => 'sometimes|array',
            'terms_services.clauses' => 'required_with:terms_service'
        ];
    }
}
