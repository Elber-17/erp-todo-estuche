<?php

namespace App\Http\Requests\Quote;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod'                    => 'required|unique:quotes,cod',
            'valid_until'            => 'required|date_format:d-m-Y',
            'date_delivery'          => 'required|date_format:d-m-Y',
            'form_pay'               => 'required',
            'status'                 => 'required',
            'details'                => 'required|array',
            'details.subtotal'       => 'required_with:details',
            'details.IVA'            => 'required_with:details',
            'details.others'         => 'sometimes',
            'details.total'          => 'required_with:details',
            'delivery_design'        => 'boolean',
            'design_our'             => 'boolean',
            'product_id'             => 'required|exists:products,id',
            'contact_id'             => 'required|exists:contacts,id',
            'terms_services'         => 'required',
            'terms_services.clauses' => 'required_with:terms_service'
        ];
    }
}
