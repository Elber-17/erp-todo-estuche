<?php

namespace App\Http\Requests\User;

use App\Entities\User;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->route()->Parameters('users');
        $user = $this->findRecordInRequest(User::class, $user_id['user']);

        return [
            'first_name'        => 'sometimes',
            'last_name'         => 'sometimes',
            'dni'               => 'sometimes|unique:people,dni,'.$user->people['id'],
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'username'          => 'sometimes|unique:users,username,'.$user['id'],
            'password'          => 'sometimes|min:6',
            'email'             => 'sometimes|unique:users,email,'.$user['id'],
            'is_active'         => 'sometimes|boolean',
            'person_id'         => 'required_without:dni|unique:users,person_id,'.$user_id['user']
        ];
    }
}
