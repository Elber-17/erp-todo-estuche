<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lead_id = $this->route()->Parameter('users');

        return [
            'first_name'        => 'required_with:dni',
            'last_name'         => 'required_with:dni',
            'dni'               => 'sometimes|unique:people',
            'phone'             => 'nullable',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'nullable',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'username'          => 'required|unique:users',
            'password'          => 'required|min:6',
            'email'             => 'nullable|email|unique:people',
            'is_active'         => 'sometimes|boolean',
            'person_id'         => 'required_without:dni'
        ];
    }
}

