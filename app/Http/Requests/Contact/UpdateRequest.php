<?php

namespace App\Http\Requests\Contact;

use App\Entities\Contact;
use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $contact_id = $this->route()->parameters('contacts');
        $contact = $this->findRecordInRequest(Contact::class, $contact_id['contact']);

        return [
            'first_name'        => 'sometimes',
            'last_name'         => 'sometimes',
            'dni'               => 'sometimes|unique:people,dni,'.$contact->people->id,
            'email'             => 'nullable|email|unique:people,email,'.$contact->people->id,
            'phone'             => 'sometimes',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'sometimes',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'source'            => 'sometimes',
            'description'       => 'sometimes',
            'person_id'         => 'required_without:dni|unique:contacts,person_id,'.$contact_id['contact'],
            'user_id'           => 'sometimes|exists:users,id',
            'company_id'        => 'sometimes|exists:companies,id'
        ];
    }
}
