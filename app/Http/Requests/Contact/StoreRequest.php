<?php

namespace App\Http\Requests\Contact;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required_with:dni',
            'last_name'         => 'required_with:dni',
            'dni'               => 'sometimes|unique:people',
            'email'             => 'nullable|email|unique:people',
            'phone'             => 'nullable',
            'phone.landline'    => 'required_with:phone',
            'phone.movil'       => 'required_with:phone',
            'address'           => 'nullable',
            'address.street'    => 'required_with:address',
            'address.country'   => 'required_with:address',
            'address.state'     => 'required_with:address',
            'address.city'      => 'required_with:address',
            'source'            => 'sometimes',
            'description'       => 'sometimes',
            'person_id'         => 'required_without:dni',
            'company_id'        => 'sometimes|exists:companies,id'
        ];
    }
}
