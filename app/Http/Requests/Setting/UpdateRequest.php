<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\ApiRequest;
use App\Rules\SettingFieldRule;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $setting_id = $this->route()->parameters('settings');

        return $this->validateFunctionModule($setting_id['setting']);
    }
}
