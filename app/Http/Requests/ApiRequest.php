<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class ApiRequest extends FormRequest
{
    /**
     * @param $resource_class
     * @param $record_id
     * @return mixed
     */
    protected function findRecordInRequest($resource_class, $record_id)
    {
        return call_user_func_array([$resource_class, 'findOrFail'], [$record_id]);
    }

    protected function validateFunctionModule($module){

        if($module == 2 || $module == 3) {
            return [
                'fields.sources' => 'required'];
        }

        if($module == 4) {
            return [
                'fields.industry' => 'required'];
        }

        if($module == 5) {
            return [
                'fields.type_pack' => 'required',
                'fields.status'     => 'required'];
        }

        if($module == 6) {
            return [
                'fields.form_pay' => 'required',
                'fields.status'   => 'required'];
        }
    }

     /**
     * Basic rules for resource index request
     * @return array
     */
    protected function indexRules()
    {
        return [
            'size'            => 'sometimes|integer|max:500'
        ];
    }

}
