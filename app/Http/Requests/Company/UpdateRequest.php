<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = $this->route()->Parameter('company');

        return [
            'name'                         => 'sometimes',
            'industry'                     => 'sometimes',
            'rif'                          => 'required|unique:companies,rif,'.$company_id,
            'phone'                        => 'sometimes',
            'phone.landline'               => 'required_with:phone',
            'phone.movil'                  => 'required_with:phone',
            'shipping_address'             => 'sometimes',
            'shipping_address.street'      => 'required_with:shipping_address',
            'shipping_address.country'     => 'required_with:shipping_address',
            'shipping_address.state'       => 'required_with:shipping_address',
            'shipping_address.city'        => 'required_with:shipping_address',
            'website'                      => 'sometimes',
        ];
    }
}
