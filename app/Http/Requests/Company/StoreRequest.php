<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                         => 'required',
            'email'                        => 'sometimes|unique:companies',
            'industry'                     => 'sometimes',
            'rif'                          => 'required|unique:companies',
            'phone'                        => 'nullable',
            'phone.landline'               => 'required_with:phone',
            'phone.movil'                  => 'required_with:phone',
            'shipping_address'             => 'required',
            'shipping_address.street'      => 'required_with:shipping_address',
            'shipping_address.country'     => 'required_with:shipping_address',
            'shipping_address.state'       => 'required_with:shipping_address',
            'shipping_address.city'        => 'required_with:shipping_address',
            'webiste'                      => 'sometimes'
        ];
    }
}
