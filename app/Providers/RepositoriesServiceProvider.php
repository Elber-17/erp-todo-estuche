<?php

namespace App\Providers;

use App\Repositories\BaseInterface;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Lead\LeadRepository;
use App\Repositories\Person\PersonInterface;
use App\Repositories\Company\CompanyRepository;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Quote\QuoteRepository;
use App\Repositories\Setting\SettingRepository;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            BaseInterface::class,
            UserRepository::class,
            PersonInterface::class,
            LeadRepository::class,
            ContactRepository::class,
            CompanyRepository::class,
            QuoteRepository::class,
            SettingRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
