<?php

namespace App\Entities;

use App\Entities\User;
use App\Entities\Quote;
use App\Entities\Person;
use App\Entities\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $table = 'contacts';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'source', 'description', 'person_id', 'user_id', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Get the person that owns the contact.
     *
     */
    public function people()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    /**
     * Get the user that owns the contact.
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the company that associed the contact.
     *
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * Get quotes belong to the contact.
     *
     */
    public function quote(){
        return $this->belongsTo(Quote::class);
    }
}
