<?php

namespace App\Entities;

use App\Entities\Lead;
use App\Entities\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $table = 'companies';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'industry', 'rif',
        'website', 'phone', 'shipping_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone'            => 'array',
        'shipping_address' => 'array'
    ];

    /**
     * Get basic company information.
     *
     * @return array
     */

    public function getBasicAttribute(){
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'address' => [
                'street'  => $this->shipping_address['street'],
                'details' => "{$this->shipping_address['city']} {$this->shipping_address['state']} {$this->shipping_address['country']}"],
            'phone' => "{$this->phone['landline']}, {$this->phone['movil']}"
        ];
    }

    /**
     * Get the leads associated with a company
     *
     */
    public function lead()
    {
        return $this->hasMany(Lead::class, 'company_id', 'id');
    }

    /**
     * Get the contacts associated with a company
     *
     */
    public function contact()
    {
        return $this->hasMany(Contact::class, 'company_id', 'id');
    }
}
