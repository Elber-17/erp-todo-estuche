<?php

namespace App\Entities;

use App\Entities\Lead;
use App\Entities\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $table = 'people';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'dni', 'email',
        'phone','picture', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'laravel_through_key'

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone'      => 'array',
        'address'    => 'array'
    ];

    /**
     * Get full name the person
     * @return string
     */
    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Get the user that owns the person.
     *
     */
    public function user()
    {
        return $this->belongsTo(UserLevel::class);
    }

    /**
     * Get the contacts associated with the person.
     *
     */
    public function contact()
    {
        return $this->hasOne(Contact::class, 'person_id', 'id');
    }

    /**
     * Get the lead associated with the person.
     *
     */
    public function lead()
    {
        return $this->hasOne(Lead::class, 'person_id', 'id');
    }


}

