<?php

namespace App\Entities;

use App\Entities\Lead;
use App\Entities\Person;
use App\Entities\Contact;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use SoftDeletes;

    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'remember_token', 'person_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    public function people()
    {
        return $this->hasOne(Person::class, 'id', 'person_id');
    }

    /**
     * Get the users associated with a contact
     *
     */
    public function contact()
    {
        return $this->hasMany(Contact::class, 'user_id', 'id');
    }

    public function lead()
    {
        return $this->hasMany(Lead::class, 'user_id', 'id');
    }
}

