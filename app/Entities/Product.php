<?php

namespace App\Entities;

use App\Entities\Quote;
use App\Entities\RawMaterial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'name', 'description', 'cost_hours_man', 'speed_production', 'elements_product',
        'cant_per_boxes', 'dimensions', 'cover_picture', 'perforations',
        'type_pack', 'status', 'raw_material_id', 'warehouse_id', 'unit_price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cost_hours_man'    => 'array',
        'speed_production'  => 'array',
        'elements_product'  => 'array',
        'dimensions'        => 'array',
        'perforations'      => 'boolean'
    ];

    /**
     * The materials that belong to the product.
     *
     */
    public function materials() {
        return $this->belongsToMany(RawMaterial::class, 'product_raw_material');
    }

    /**
     * Get the warehouses that saved the products.
     *
     */
    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');

    }

     /**
     * Get quotes associated with a product.
     *
     */
    public function quote(){
        return $this->belongsTo(Quote::class);
    }

}
