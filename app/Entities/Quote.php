<?php

namespace App\Entities;

use App\Entities\Person;
use App\Entities\Company;
use App\Entities\Contact;
use App\Entities\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
    use SoftDeletes;

    protected $table = 'quotes';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod', 'valid_until', 'date_delivery', 'form_pay', 'status',
        'details', 'delivery_design', 'design_our', 'product_id', 'contact_id',
        'terms_services'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'valid_until'    => 'datetime',
        'date_delivery'  => 'datetime',
        'details'        => 'array',
        'terms_services' => 'array',
        'terms_services.clauses' => 'array'
    ];

    /**
     * Get the contact associated with a quotes.
     *
     */
    public function contact() {
        return $this->hasMany(Contact::class, 'id', 'contact_id');
    }

    /**
     * Get the product associated with a quotes.
     *
     */
    public function product() {
        return $this->hasMany(Product::class, 'id', 'product_id');
    }

    /**
     * Get the contact owner.
     *
     */
    public function contactPerson(){
        return $this->hasOneThrough(Person::class, Contact::class, 'id', 'id', 'contact_id', 'person_id');
    }

    /**
     * Get the company contact.
     *
     */
    public function contactCompany(){
        return $this->hasOneThrough(Company::class, Contact::class, 'id', 'id', 'contact_id', 'company_id');
    }
}

