<?php

namespace App\Repositories\Lead;

use App\Entities\Lead;
use App\Repositories\BaseInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\Lead\LeadResource;
use App\Http\Resources\Lead\LeadCollection;
use App\Repositories\Person\PersonRepository;

class LeadRepository implements BaseInterface {

    protected $model;
    protected $lead;
    protected $person;

    /**
     * Lead Repository constructor.
     *
     * @param Lead $lead
     * @param PersonRepository $person
     */
    public function __construct(Lead $lead, PersonRepository $person)
    {
        $this->model = $lead;
        $this->person = $person;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;

        return new LeadCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new_lead = new Lead();
        $new_lead->fill($data);

        if (!isset($data['person_id']))
        {
            $new_person = $this->person->create($data);
            $new_lead->person_id = $new_person->id;
        }

        $new_lead->user_id = 1;
        $new_lead->save();

        return new LeadResource($this->model::findOrFail($new_lead->id));
    }
    public function getById($id)
    {
        return new LeadResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        $lead = $this->getById($id);
        $this->person->destroy($lead['person_id']);

        return $lead->delete();;
    }

    public function UpdateById($id, array $data)
    {
        $lead = $this->getById($id);
        $this->person->UpdateById($lead['person_id'], $data);
        $lead->fill($data);
        $lead->save();

        return new LeadResource($lead);
    }
}
