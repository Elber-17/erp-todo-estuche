<?php

namespace App\Repositories\Quote;

use App\Entities\Quote;
use App\Http\Resources\Quote\QuoteResource;
use App\Http\Resources\Quote\QuoteCollection;
use App\Repositories\BaseInterface;

class QuoteRepository implements BaseInterface {

    protected $model;
    protected $quote;

    /**
     * Quote Repository constructor.
     * @param Quote $quote
     */
    public function __construct(Quote $quote)
    {
        $this->model = $quote;
    }

    public function getAll($data){

        $page = !empty($data['size']) ? (int)$data['size'] : 10;

        return new QuoteCollection($this->model::paginate($page));
    }

    public function getById($id)
    {
        return new QuoteResource($this->model::findOrFail($id));
    }

    public function create(array $data){
        $new_quote = new Quote();
        $new_quote->fill($data);
        $new_quote->save();

        return $this->getById($new_quote->id);
    }

    public function destroy($id){
        return $this->model::find($id)->delete();
    }

    public function UpdateById($id, array $data){
        $quote = $this->model::findOrFail($id);
        $quote->fill($data);
        $quote->save();

        return new QuoteResource($quote);
    }

}
