<?php

namespace App\Repositories\Contact;

use App\Entities\Lead;
use App\Entities\Contact;
use App\Repositories\BaseInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Person\PersonRepository;
use App\Http\Resources\Contact\ContactResource;
use App\Http\Resources\Contact\ContactCollection;

class ContactRepository implements BaseInterface {

    protected $model;
    protected $contact;
    protected $person;

    /**
     * Contact Repository constructor.
     *
     * @param Contact $contact
     * @param PersonRepository $person
     */
    public function __construct(Contact $contact, PersonRepository $person)
    {
        $this->model = $contact;
        $this->person = $person;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;

        return new ContactCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new_contact = new Contact();
        $new_contact->fill($data);

        if (!isset($data['person_id']))
        {
            $new_person = $this->person->create($data);
            $new_contact->person_id = $new_person->id;
        }

        $new_contact->user_id = 1;
        $new_contact->save();

        return new ContactResource($this->model::findOrFail($new_contact->id));
    }

    public function getById($id)
    {
        return new ContactResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        $contact = $this->getById($id);
        $this->person->destroy($contact['person_id']);

        return $contact->delete();
    }

    public function UpdateById($id, array $data)
    {
        $contact = $this->getById($id);
        $this->person->UpdateById($contact['person_id'], $data);
        $contact->fill($data);
        $contact->save();

        return new ContactResource($contact);
    }

    public function transformContact($id)
    {
        $lead = Lead::where('id', $id)
        ->get(['source', 'description', 'person_id', 'user_id', 'company_id'])
        ->toArray();

        $contact = $this->create($lead[0]);
        Lead::findOrFail($id)->delete();

        return new ContactResource($this->model::findOrFail($contact->id));
    }
}
