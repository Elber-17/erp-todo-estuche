<?php

namespace App\Repositories\Product;

use App\Entities\Product;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductCollection;
use App\Repositories\BaseInterface;

class ProductRepository implements BaseInterface {

    protected $model;
    protected $product;

    /**
     * Product Repository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function getAll($data){

        $page = !empty($data['size']) ? (int)$data['size'] : 10;

        return new ProductCollection($this->model::paginate($page));
    }

    public function getById($id)
    {
        return new ProductResource($this->model::findOrFail($id));
    }

    public function create(array $data){

        $new_product = new Product();
        $new_product->fill($data);
        $new_product->save();
        $this->SyncMaterials($new_product->id, $data['raw_materials']);
        return new ProductResource($new_product);
    }

    public function destroy($id){
        return $this->model::find($id)->delete();
    }

    public function UpdateById($id, array $data){
        $product = $this->model::findOrFail($id);
        $product->fill($data);
        $product->save();
        $this->SyncMaterials($id, $data['raw_materials']);

        return new ProductResource($product);
    }

    public function SyncMaterials($id, array $array) {
        return $this->model::find($id)->materials()->sync($array);
    }
}
