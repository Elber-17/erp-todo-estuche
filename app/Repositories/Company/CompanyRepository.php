<?php

namespace App\Repositories\Company;

use App\Entities\Company;
use App\Repositories\BaseInterface;
use App\Http\Resources\Company\CompanyResource;
use App\Http\Resources\Company\CompanyCollection;

class CompanyRepository implements BaseInterface {

    protected $model;
    protected $company;

    /**
     * Company Repository constructor.
     *
     * @param Company $company
     */

    public function __construct(Company $company)
    {
        $this->model = $company;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;

        return new CompanyCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new = $this->model::create($data);

        return new CompanyResource($new);
    }

    public function getById($id)
    {
        return new CompanyResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $this->model::findOrFail($id)->update($data);

        return new CompanyResource($this->model::find($id));
    }
}
