<?php

namespace App\Repositories\Setting;

interface SettingInterface {

    public function getModules();
    public function getById($id);
    public function UpdateById($id, array $data);
}
