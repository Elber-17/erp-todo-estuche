<?php

namespace App\Repositories\Setting;

use App\Entities\Setting;
use App\Repositories\Setting\SettingInterface;

class SettingRepository implements SettingInterface {

    protected $model;
    protected $setting;

    /**
     * Setting Repository constructor.
     * @param Setting $setting
     */
    public function __construct(Setting $setting)
    {
        $this->model = $setting;
    }

    public function getModules() {
        return Setting::get(['id', 'module']);
    }

    public function getById($id)
    {
        return Setting::findOrFail($id);
    }

    public function UpdateById($id, array $data){
        $setting = $this->model::findOrFail($id);
        $setting->fill($data);
        $setting->save();

        return Setting::findOrFail($id);
    }
}
