<?php

namespace App\Repositories\User;

use App\Entities\User;
use App\Repositories\BaseInterface;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UserCollection;
use App\Repositories\Person\PersonRepository;

class UserRepository implements BaseInterface {

    protected $model;

    /**
     * User Repository constructor.
     * @param User $user
     */
    public function __construct(User $user, PersonRepository $person)
    {
        $this->model = $user;
        $this->person = $person;
    }

    public function getAll($data)
    {
        $page = !empty($data['size']) ? (int)$data['size'] : 10;
        return new UserCollection($this->model::paginate($page));
    }

    public function create(array $data)
    {
        $new_user = new User();
        $new_user->fill($data);

        if (!isset($data['person_id']))
        {
            $new_person = $this->person->create($data);
            $new_user->person_id = $new_person->id;
        }
        $new_user->password = bcrypt($data['password']);
        $new_user->is_active = 1;
        $new_user->save();

        return new UserResource($new_user);
    }
    public function getById($id)
    {
        return new UserResource($this->model::findOrFail($id));
    }

    public function destroy($id)
    {
        $getUser = $this->getById($id);
        $this->person->destroy($getUser['person_id']);

        return $this->model::findOrFail($id)->delete();
    }

    public function UpdateById($id, array $data)
    {
        $user = $this->getById($id);
        $user->fill($data);

        $this->person->UpdateById($user['person_id'], $data);
        if (isset($data['password']))
        {
            $user['password'] = bcrypt($data['password']);
        }

        $user->save();

        return new UserResource($this->model::find($id));
    }
}
