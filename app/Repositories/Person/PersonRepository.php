<?php

namespace App\Repositories\Person;

use App\Entities\Person;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Person\PersonInterface;

class PersonRepository implements PersonInterface {

    protected $model;
    protected $person;

    /**
     * Person Repository constructor.
     * @param Person $person
     */
    public function __construct(Person $person)
    {
        $this->model = $person;
    }

    public function create(array $data){
       return $this->model::create($data);
    }

    public function destroy($id){
        return $this->model::find($id)->delete();
    }

    public function UpdateById($id, array $data){

        return $this->model::findOrFail($id)->update($data);
    }
}
