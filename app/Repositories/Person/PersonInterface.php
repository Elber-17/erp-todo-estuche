<?php

namespace App\Repositories\Person;

interface PersonInterface {

    public function create(array $data);
    public function destroy($id);
    public function UpdateById($id, array $data);
}
