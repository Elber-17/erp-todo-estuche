<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('source')->default('Web');
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreignId('person_id')
            ->constrained('people')
            ->nullable()
            ->onDelete('cascade');

            $table->foreignId('user_id')
            ->constrained('users')
            ->nullable()
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
