<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->string('cod');
            $table->dateTime('valid_until');
            $table->dateTime('date_delivery');
            $table->string('form_pay');
            $table->string('status');
            $table->json('details')->nullable();
            $table->boolean('delivery_design')->default(0);
            $table->boolean('design_our')->default(0);
            $table->integer('product_id')->unsigned()->index();
            $table->json('terms_services');
            $table->foreignId('contact_id')
            ->nullable()
            ->constrained('contacts')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
