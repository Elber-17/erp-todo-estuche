<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAddForeignOnLeadsAndContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->foreignId('company_id')
            ->nullable()
            ->constrained('companies')
            ->onDelete('cascade');
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->foreignId('company_id')->nullable()
            ->constrained('companies')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
        });
    }
}
