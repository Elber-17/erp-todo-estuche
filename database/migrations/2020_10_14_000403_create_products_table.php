<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod');
            $table->string('name');
            $table->string('description')->nullable();
            $table->json('cost_hours_man');
            $table->json('speed_production');
            $table->json('elements_product');
            $table->integer('cant_per_boxes');
            $table->json('dimensions');
            $table->string('cover_picture')->nullable();
            $table->boolean('perforations')->default(0);
            $table->double('unit_price');
            $table->string('type_pack'); //tipo de empaque provienente de una tabla setting
            $table->string('status'); // Estado del producto proveniente de una tabla setting
            $table->integer('warehouse_id')->nullable(); // almacen en donde esta ubicado.
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
