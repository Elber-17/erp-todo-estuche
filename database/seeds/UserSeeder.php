<?php

use App\Entities\User;
use App\Entities\Person;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::Table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),
            'is_active' => 1,
            'person_id' => factory(Person::class)->create()->id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        factory(User::class, 5)->create();
    }
}
