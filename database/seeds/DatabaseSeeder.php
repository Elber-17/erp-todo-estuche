<?php

use UserSeeder as UserData;
use LeadSeeder as LeadData;
use ContactSeeder as ContactData;
use CompanySeeder as CompanyData;
use RawMaterialSeeder as MaterialData;
use WareHouseSeeder as WareData;
use ProductSeeder as ProductData;
use QuoteSeeder as QuoteData;
use SettingSeeder as SettingData;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CompanyData::class,
            UserData::class,
            ContactData::class,
            LeadData::class,
            WareData::class,
            ProductData::class,
            MaterialData::class,
            QuoteData::class,
            SettingData::class,
        ]);
    }
}
