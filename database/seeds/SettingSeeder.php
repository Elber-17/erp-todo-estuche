<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'id' => 1,
                'module' => 'users',
                'fields' => json_encode([])
            ],
            [
                'id' => 2,
                'module' => 'leads',
                'fields' => json_encode(array(
                    'sources' => [[
                        'name' => 'Web',
                        'description' => 'Proveniente de la Web'
                    ],
                    [
                        'name' => 'Llamada',
                        'description' => 'Proveniente de una llamada'
                    ]]
                ))
            ],
            [
                'id' => 3,
                'module' => 'contacts',
                'fields' => json_encode(array(
                    'sources' => [[
                        'name' => 'Web',
                        'description' => 'Proveniente de la Web'
                    ],
                    [
                        'name' => 'Llamada',
                        'description' => 'Proveniente de una llamada'
                    ]]
                ))
            ],
            [
                'id' => 4,
                'module' => 'company',
                'fields' => json_encode(array(
                    'industry' => [[
                        'name' => 'IT',
                        'description' => 'Tecnologia'
                    ],
                    [
                        'name' => 'Banca',
                        'description' => 'Banca'
                    ]]
                ))
            ],
            [
                'id' => 5,
                'module' => 'products',
                'fields' => json_encode(array(
                    'type_pack' => [
                        [
                        'name' => 'Fondo Automatico'
                        ],
                        [
                            'name' => 'Solapa arriba'
                        ],
                        [
                            'name' => 'Bandeja'
                        ],
                        [
                            'name' => '4 esquinas'
                        ],
                        [
                            'name' => 'Bandeja Auto Armable'
                        ]
                    ],
                        'status' => [
                            [
                                'name' => 'Activo'
                            ],
                            [
                                'name' => 'Desactivado'
                            ]
                        ]
                ))
            ],
            [
                'id' => 6,
                'module' => 'quotes',
                'fields' => json_encode(array(
                    'form_pay' => [[
                        'name' => 'Contado'
                    ],
                    [
                        'name' => 'Credito'
                    ]]
                ))
            ],

            ]);
    }
}
