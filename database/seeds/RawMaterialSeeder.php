<?php

use App\Entities\Product;
use App\Entities\RawMaterial;
use Illuminate\Database\Seeder;

class RawMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(RawMaterial::class, 30)->create();

    }
}
