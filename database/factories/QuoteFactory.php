<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Quote;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Quote::class, function (Faker $faker) {
    $cant1 = $faker->numberBetween(500, 3000);
    $unit1 = $faker->randomFloat(2, 100);
    $cant2 = $faker->numberBetween(1, 3);
    $unit2 = $faker->randomFloat(2, 20);
    $cant3 = $faker->numberBetween(1, 3);
    $unit3 = $faker->randomFloat(2, 20);
    $sub = ($unit1+$unit2+$unit3);

    return [
        'cod'           => 'TE-VE-FO-00'.$faker->unique()->randomDigit,
        'valid_until'   => $dely = Carbon::now()->format('d-m-Y'),
        'date_delivery' => $valid = Carbon::now()->addDay(2)->format('d-m-Y'),
        'form_pay'      => $form = $faker->randomElement(['Contado', 'Pre-pagado', 'A credito']),
        'status'        => $faker->randomElement([true, false]),
        'details'       => [
            'product'   => array(
                'name'       => $faker->word,
                'cant'       => $cant1,
                'unit_price' => $unit1),
            'procedure'      => array(
                'description' => $faker->paragraph(1),
                'unit_price'  => $unit2,
                'cant'        => $cant2),
            'services'        => array(
                'description' => $faker->paragraph(1),
                'unit_price'  => $unit3,
                'cant'        => $cant3),
            'subtotal'        => $sub,
            'IVA'             => ($sub*16/100),
            'others'          => $faker->word],
            'design_our'      => $faker->randomElement([true, false]),
            'delivery_design' => $faker->randomElement([true, false]),
            'terms_services'  => array(
                'form_pay'      => $form.', al momento de la aprobacion',
                'delivery_date' => $dely,
                'valid_until'   => $valid,
                'clauses'       => array(
                    'one'   => $faker->paragraph(1),
                    'two'   => $faker->paragraph(1),
                    'three' => $faker->paragraph(1)),
                ),
            'contact_id'      => $faker->numberBetween(1, 2),
            'product_id'      => $faker->numberBetween(1, 10),
    ];
});
