<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\User;
use App\Entities\Person;
use App\Entities\Company;
use App\Entities\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'source'      => $faker->domainWord,
        'description' => $faker->paragraph(1),
        'created_at'  => now(),
        'updated_at'  => now(),
        'person_id'   => function() {
            return factory(Person::class)->create()->id;
        },
        'user_id'     => User::all()->random()->id,
        'company_id'  => Company::all()->random()->id,
    ];
});
