<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\User;
use App\Entities\Person;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username'  => $faker->userName,
        'password'  => '$2y$10$fP5cJD7g0.GQa9lAaPjiLOmohpu0sikicqJSkk2JmzSUxMsb/uZIe', // secret
        'is_active' => true,
        'remember_token' => Str::random(10),
        'person_id'   => function() {
            return factory(Person::class)->create()->id;
        },
        'created_at' => now(),
        'updated_at' => now()
    ];
});
