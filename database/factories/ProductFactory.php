<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
            'cod'               => 'P-TE-ET-MOD1-0'.$faker->randomDigit,
            'name'              => $faker->word,
            'description'       => $faker->sentence(5),
            'cost_hours_man' => [
                'print'      => $faker->numberBetween(2000, 6500),
                'troquel'    => $faker->numberBetween(2000, 6500),
                'gummed'     => $faker->numberBetween(2000, 6500),
                'operative'  => $faker->numberBetween(2000, 6500)],
            'speed_production' => [
                'print'        => $faker->numberBetween(2000, 6500),
                'troquel'      => $faker->numberBetween(2000, 6500),
                'gummed'       => $faker->numberBetween(2000, 6500)],
            'elements_product' => [
                'relieve'          => $faker->word,
                'troquel'          => $faker->numerify('##'),
                'caliber'          => $faker->numerify('##'),
                'type_gummed'      => $faker->randomElement(['Lineal', 'Por punto'], 1),
                'sheet_fit'        => $faker->word,
                'unit_measurement' => 'Unidad',
                'varnished'        => $faker->randomElement([1, 0], 1),
                'colors'           => $faker->randomElement(['4 Colores(Rojo, Azul, Morado, Verde)', 'Verde', 'Azul'], 1),
                'others'           => $faker->word],
            'cant_per_boxes'  => $faker->numberBetween(500, 3000),
            'unit_price' => $faker->randomFloat(2, 100),
            'dimensions' => [
                'form'   => $faker->randomElement(['Rectangular', 'Cuadrada', 'Cubica'], 1),
                'width'  => $faker->numerify('###MM'),
                'height' => $faker->numerify('###MM'),
                'length' => $faker->numerify('###MM')],
            'cover_picture' => $faker->imageUrl($width = 640, $height = 480),
            'perforations'  => $faker->randomElement([true, false]),
            'type_pack'     => $faker->randomElement(['Fondo Automatico', 'Solapa Arriba', 'Solapa abajo', 'Bandeja 4 Esquinas', 'Bandeja Auto-Armable', 'Otro Tipo'], 1),
            'status'        => $faker->randomElement([true, false]),
            'warehouse_id'  => $faker->numberBetween(1, 4),
    ];
});

