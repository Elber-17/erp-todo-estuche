<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\RawMaterial;
use Faker\Generator as Faker;

$factory->define(RawMaterial::class, function (Faker $faker) {
    return [
        'cod'        => 'P-TE-ET-MOD1-00'.$faker->unique()->numberBetween(1, 50),
        'name'       => $faker->unique()->word,
        'quantity'   => $faker->randomDigit,
    ];
});
