<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Lead;
use App\Entities\User;
use App\Entities\Person;
use App\Entities\Company;
use Faker\Generator as Faker;

$factory->define(Lead::class, function (Faker $faker) {
    return [
        'source'      => $faker->domainWord,
        'description' => $faker->paragraph(1),
        'created_at'  => now(),
        'updated_at'  => now(),
        'user_id'     => User::all()->random()->id,
        'company_id'  => Company::all()->random()->id,
        'person_id'   => function() {
            return factory(Person::class)->create()->id;
        }
    ];
});
