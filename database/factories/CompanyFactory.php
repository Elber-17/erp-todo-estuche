<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name'     => $faker->company,
        'email'    => $faker->companyEmail,
        'industry' => $faker->word,
        'rif'      => $faker->unique()->ean8,
        'website'  => $faker->domainName,
        'phone'    => [
            'landline' => $faker->e164PhoneNumber,
            'movil'    => $faker->e164PhoneNumber],
        'shipping_address' => [
            'street'  => $faker->address,
            'country' => $faker->country,
            'state'   => $faker->state,
            'city'    => $faker->city]
    ];
});

