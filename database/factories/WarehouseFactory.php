<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Warehouse;
use Faker\Generator as Faker;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        'cod'  => 'TE-TD-00'.$faker->unique()->randomDigit,
        'barn' => $faker->citySuffix,
        'zone' => $faker->cityPrefix,
        'area' => $faker->cityPrefix,
    ];
});
