<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName($gender = 'male'|'female'),
        'last_name'  => $faker->lastname,
        'dni'        => $faker->unique()->ean8,
        'email'      => $faker->unique()->safeEmail,
        'phone'      => [
            'landline' => $faker->e164PhoneNumber,
            'movil'    => $faker->e164PhoneNumber],
        'picture'     => $faker->imageUrl($width = 640, $height = 480),
        'address'     => [
            'street'  => $faker->address,
            'country' => $faker->country,
            'state'   => $faker->state,
            'city'    => $faker->city]
    ];
});
